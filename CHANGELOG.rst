python-lexer-solidity change log
================================

[vX.Y.Z] - Unreleased
---------------------
* Added minimal instructions in README.

[v0.0.2] - 2017-04-27
---------------------
* Changed license from MIT to BSD.

[v0.0.1] - 2017-04-27
---------------------
* Ported ``pygments-main`` `PR #626`_ to this package.

.. _PR #626: https://bitbucket.org/birkenfeld/pygments-main/pull-requests/626/add-solidity-lexer
